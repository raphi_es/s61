from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse
from selenium import webdriver
from .forms import Input_Form
from .models import Matkul

@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    def SetUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.get('http://localhost:8000')

    def tearDown(self):
        self.selenium.quit

class MainTestCase(TestCase):
    def test_root_url_status_200(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        # You can also use path names instead of explicit paths.
        response = self.client.get(reverse('main:home'))
        self.assertEqual(response.status_code, 200)

    def test_root_url_template(self):
        response = Client.get('/')
        self.assertTemplateUsed(response, 'home.html')
    
    def test_readmatkul_url_template(self):
        response = Client.get('/readmatkul')
        self.assertTemplateUsed(response, 'readmatkul.html')
    
    def test_forms_is_valid(self):
        form_data = {'display_name': 'something', 'sks': '2'}
        form = Input_Form(data=form_data)
        self.assertTrue(form.is_valid())
        self.assertEqual(form.cleaned_data['display_name'],'something')

    def test_model_Matkul(self):
        Matkul.objects.create(display_name = "Budi", sks="2")
    


    


class MainFunctionalTestCase(FunctionalTestCase):
    def SetUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.get('http://localhost:8000')

    def tearDown(self):
        self.selenium.quit

    def test_root_url_exists(self):
        self.selenium.get(f'{self.live_server_url}/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())
