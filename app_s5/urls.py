from django.urls import path
from django.conf.urls import url
from . import views
from .views import savename, readmatkul
app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('savename', savename),
    path('readmatkul/', views.readmatkul, name='readmatkul'),
    #url(r'^delete/(?P<part_id>[0-9]+)/$', views.delete_matkul, name='delete_view'),
    path('delete/<pk>', views.matkul_delete, name='matkul_delete'),
    path('detail/<pk>', views.detailmatkul, name='detailmatkul'),
    path('detail_matkul/', views.detail_matkul, name='detail_matkul')
]
